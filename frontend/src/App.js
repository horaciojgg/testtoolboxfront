import React from 'react';
import './App.css';
import FormBasicClass from "./components/FormBasicClass";
import HeroBanner from "./components/HeroBanner";


function App() {
  return (
    <div className="App">
      <HeroBanner></HeroBanner>
      <FormBasicClass></FormBasicClass>
    </div>
  );
}


export default App;
