import * as actions from "./actionTypes";

const initialState = {
 text: "",
 loading: false,
 showModal: false
};

export default function reducer(state = initialState, action) {
    if (typeof state === 'undefined') {
        return initialState
      }
    switch (action.type) {
        case actions.TEXT_POSTED:
            return {...state, text: action.payload.text};
        case actions.START_POST:
            return {...state, loading: true}
        case actions.FINISH_POST:
            return {...state, loading: false};
        case actions.POST_ERROR:
            return {...state, loading: false};
        case actions.SHOW_MODAL:
            return {...state, showModal: true};
        case actions.HIDE_MODAL:
            return {...state, showModal: false};
        default:
             return state;
    }

}