import React from "react";
import { Button, Form, Modal } from "react-bootstrap"
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducer from "../reducer";
import * as actions from "../actionTypes"
import axios from "axios";
import '../App.css';



class FormBasicClass extends React.Component {
 

constructor(props) {
    super(props);
    this.handleMouseMove = this.handleMouseMove.bind(this);
    this.handleClose = this.handleClose.bind(this)
    this.postToApi = this.postToApi.bind(this);
    this.store = createStore(reducer, applyMiddleware(thunk));
    this.wrapper = React.createRef();
  }

  handleMouseMove() {
    this.setState({showModal: false });
  }

  postToApi(event) {
    event.preventDefault();
    this.store.dispatch({type:actions.START_POST})
    const item = {
      text: this.store.getState().text,
    };
    axios
      .post('http://localhost:3000/text', item)
      .then((response) => {
          this.store.dispatch({type: actions.FINISH_POST})
          this.store.dispatch({type: actions.SHOW_MODAL})

      })
      .catch(err => {
        this.store.dispatch({type: actions.SHOW_MODAL})
      });
  }

  handleClose() {
    this.store.dispatch({type: actions.HIDE_MODAL})
  }

  render() {
    return (
      <div ref={this.wrapper} className="main-form" onMouseMove={this.handleMouseMove}>
        <Modal show={this.store.getState().showModal} onHide={this.handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Here's your response from the server.</Modal.Title>
        </Modal.Header>
        <Modal.Body>{this.store.getState().text ? this.store.getState().text : "Whoops! Looks like you sent an empty string. Try again." }</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>

      <section className="spikes"></section>
      <div className="form d-flex align-items-center justify-content-center">
      <Form onSubmit={this.postToApi}>
            <Form.Group controlId="formBasicEmail">
                <h2>You can type anything here.</h2>
                <Form.Control required  onChange={e => {
                  this.store.dispatch({type: actions.TEXT_POSTED, payload: {text: e.target.value}})
                }}  type="text" placeholder="Type your text here." />
                <Form.Text className="text-muted">
                    Don't leave it blank! You might get an error.
                </Form.Text>
            </Form.Group>

            <Button onClick={this.postToApi} variant="primary" type="submit">
                Send it!
            </Button>
        </Form>

      </div>
      <section className="triangle"></section>
      </div>
    );
  }}

export default FormBasicClass;