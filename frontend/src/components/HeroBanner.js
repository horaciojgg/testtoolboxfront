import React from "react";
import '../App.css';

export default function HeroBanner() {
    return (
      <div className="heroBanner d-flex flex-column align-items-center justify-content-center">
          <div className=""><h1 className="title">Welcome!</h1></div>
          <div>
            <h2>This is a test developed by Horacio González for Toolbox.</h2>
          </div>
      </div>
    )
}