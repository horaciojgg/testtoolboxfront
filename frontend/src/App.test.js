import React from 'react';
import ReactDOM from 'react-dom';
import FormBasicClass from "./components/FormBasicClass";
import HeroBanner from "./components/HeroBanner";

import { render } from '@testing-library/react';
import App from './App';



it('Renders main form correctly', () => {
  const div = document.createElement("div");
  ReactDOM.render(<FormBasicClass></FormBasicClass>, div)
})


it('Renders Hero Banner correctly', () => {
  const div = document.createElement("div");
  ReactDOM.render(<HeroBanner></HeroBanner>, div)
})