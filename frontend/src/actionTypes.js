export const TEXT_POSTED = "TEXT_POSTED";
export const START_POST = "START_POST";
export const FINISH_POST = "FINISH_POST"
export const POST_ERROR = "POST_ERROR";
export const SHOW_MODAL = "SHOW_MODAL";
export const HIDE_MODAL = "HIDE_MODAL";